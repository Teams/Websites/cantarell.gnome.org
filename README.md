# Cantarell typeface family website

This projects is the *website*. If you're interested in the typeface and fonts themselves, see [Cantarell-fonts](https://gitlab.gnome.org/GNOME/cantarell-fonts).

The website is live at [cantarell.gnome.org](https://cantarell.gnome.org)
